import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="latexUnifolder",
    version="0.1",
    author="Adrian Roth",
    author_email="roth.adrian@protonmail.com",
    description="Package for putting multiple latex files and external dependencies such as images into a single folder.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/roth.adrian/latex-unifolder",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points = {
            "console_scripts": [
                "latexUnifolder = latexUnifolder.__main__:main",
            ]
    },
    install_requires = [],
    python_requires='>=3.6',
)
