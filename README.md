# Latex unifolder
Ever had a latex document with images and files in a mess all over the computer.
Unifold it all into a single folder of your choice, maybe to upload it to overleaf.

usage:
latex-unifolder.py \<latex-root-file\> \[-o \<output directory\>\]
