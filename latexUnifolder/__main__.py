import argparse
import os
import os.path as osp
import shutil

from . import LatexUniFolderer

def main():
    parser = argparse.ArgumentParser(description='Unify a latex root files images and other files into a single document. For each file the filename should be in the last "{}" brackets of the line.')
    parser.add_argument('path', type=str, 
                        help='Input root latex file.')
    parser.add_argument('--output', '-o', type=str, const=None,
                        help='Optional unified output directory, default is "unifolder" in same directory as latex file.')
    parser.add_argument('--no-inline', '-i', action='store_true',
                        help='Do not inline latex files, default is to inline.')
    parser.add_argument('--extra-files', nargs='+', type=str,
                        help='Additionally files and folders to copy that are not found by the parser.')
    parser.add_argument('--extra-resources', action='store_true',
                        help='Also look for resources such as documentclasses (such as .cls), packages (such as .sty) and bibliographystyles (such as .bst).')
    args = parser.parse_args()

    if not osp.splitext(args.path)[1] == '.tex':
        raise ValueError("Path must be a .tex file")

    directory = osp.dirname(osp.realpath(args.path))
    if args.output is None:
        args.output = osp.join(directory, 'unifolder')
    if osp.isdir(args.output):
        shutil.rmtree(args.output)
    os.mkdir(args.output)
    root_base = osp.basename(args.path)
    out_path = osp.join(args.output, root_base)

    if not args.extra_files is None:
        for f in args.extra_files:
            if osp.isfile(f):
                shutil.copy(f, args.output)
            elif osp.isdir(f):
                shutil.copytree(f, osp.join(args.output, osp.basename(f)))
    
    luf = LatexUniFolderer(args.path, not args.no_inline, args.extra_resources)
    luf.unifold(out_path)

if __name__ == '__main__':
    main()
