#!/usr/bin/env python3
import json
import os.path as osp
import shutil
import re

class LatexUniFolderer:
    # Extensions for latex like files that can be parsed similarily
    _tex_ext = ['.tex', '.cls', '.sty']
            
    def __init__(self, fname, inline=True, extra_resources=True):
        self.fname = fname
        self.inline = inline
        self.extra_resources = extra_resources
        self.parsed = None
        self.is_latex_file = osp.splitext(fname)[1] in LatexUniFolderer._tex_ext

        self.document = LatexUniFolderer._read_file(fname)
        if not self.document:
            raise ValueError('Latex file {} not found'.format(fname))

    def _copy_resource(resource, outdir):
        if not resource['active']:
            return
        fname = resource['in_filename']
        if osp.isfile(fname):
            resource['out_filename'] = osp.join(outdir, resource['out_filename'])
            shutil.copyfile(fname, resource['out_filename'])
        else:
            print('Warning: File "{}" could not be found'.format(fname))

    def unifold(self, outfile):
        outdir = osp.dirname(outfile)
        parsed = self.parse()

        with open(outfile, 'w') as f:
            f.write('\n'.join(parsed['document']))
        
        resources = parsed['resources'].copy()
        for r in resources:
            LatexUniFolderer._copy_resource(r, outdir)
        with open(osp.join(outdir, 'resources.json'), 'w') as f:
            f.write(json.dumps(parsed['resources']))

        return outfile


    def _read_file(fname):
        if osp.isfile(fname):
            with open(fname) as f:
                content = f.readlines()
            if len(content) == 0:
                content = ['']
            content = [row.strip('\n') for row in content]
            return content
        else:
            return False

    def _parse_rows(self, rows, patterns):
        for row in rows:
            comment_ind = row.find('%')
            if comment_ind == -1:
                comment_ind = len(row) + 1
            for t, p in patterns.items():
                for match in re.finditer(p['re'], row):
                    if match.end() < comment_ind:
                        # print('MATCH', t, match.groups())
                        row = self._match_action(t, match, row, **p['kwargs'])
            self.parsed['document'].append(row)

    def parse(self, force=False):
        if not self.parsed is None and not force:
            return self.parsed
        self.parsed = {
                'document': [],
                'resources': []
            }

        patterns = LatexUniFolderer._basic_patterns
        if self.extra_resources:
            patterns.update(LatexUniFolderer._extra_patterns)

        if self.is_latex_file:
            self._parse_rows(self.document, patterns)
        else:
            self.parsed['document'] = self.document
        return self.parsed


    # Which extensions are allowed for each command type
    _possible_ext = {
                'documentclass': ['.cls'],
                'requirepackage': ['.sty'],
                'usepackage': ['.sty'],
                'input': ['any'],
                'inputiffileexists': ['any'],
                'include': ['.pdf'],
                'includegraphics': ['.pdf', '.png', '.jpg'],
                'bibliography': [".bib"],
                'bibliographystyle': [".bst"],
            }
    def _find_hidden_ext(fname, exts):
        ext = osp.splitext(fname)[1]
        if ext == '':
            for ext in exts:
                if osp.isfile('{}{}'.format(fname, ext)):
                    return ext, True
        elif ('any' in exts or ext in exts) and osp.isfile(fname):
            return '', True
        return '', False

    # t is the type of action
    def _match_action(self, t, match, row, recursion=False):
        fname = match.groups()[-1].strip()
        ## Add posibility of multiple files in the same action brackets

        resource = { 'type': t }
        # file is the name found in the latex file (possibly without extension)
        resource['in_file'] = fname
        resource['out_file'] = osp.basename(fname)
        ext, found = LatexUniFolderer._find_hidden_ext(fname, LatexUniFolderer._possible_ext[t])
        # filename is always with extension
        resource['in_filename'] = '{}{}'.format(resource['in_file'], ext)
        resource['out_filename'] = '{}{}'.format(resource['out_file'], ext)
        resource['ext'] = ext
        resource['active'] = found

        if recursion and found:
            # If the file is not latex syntax the document is just copied in parse
            luf_child = LatexUniFolderer(resource['in_filename'], self.inline, self.extra_resources)
            parsed = luf_child.parse()
            self.parsed['resources'].extend(parsed['resources'])
            # If the file is inlined the replace action should do nothing
            if t == 'input' and self.inline:
                row = row.replace(match.group(0), '\n'.join(parsed['document']))
                resource['active'] = False
                if not match.group(1) is None:
                    print('Warning: Parameters not taken into account when inlining {}'.format(repr(match.group(0))))
        self.parsed['resources'].append(resource)

        if resource['active']:
            replacement = match.group(0).replace(resource['in_file'], resource['out_file'])
            row = row.replace(match.group(0), replacement)
        return row

    def _latex_re_string(command, with_opts=True):
        extra = ''
        if with_opts:
            extra = r'(\[.*\])?'
        re_string = r'\\' + command + extra + r'{([\w\-,\./ ]*)\}'
        return re_string
    _basic_patterns = {
            'input': { 're': re.compile(_latex_re_string('input')), 'kwargs': {'recursion': True}},
            'include': { 're': re.compile(_latex_re_string('include')), 'kwargs': {}},
            'includegraphics': { 're': re.compile(_latex_re_string('includegraphics')), 'kwargs': {}},
            'bibliography': { 're': re.compile(_latex_re_string('bibliography')), 'kwargs': {}},
        }
    _extra_patterns = {
            'documentclass':  { 're': re.compile(_latex_re_string('documentclass')), 'kwargs': {'recursion': True}},
            'requirepackage': { 're': re.compile(_latex_re_string('RequirePackage')), 'kwargs': {'recursion': True}},
            'usepackage':     { 're': re.compile(_latex_re_string('usepackage')), 'kwargs': {'recursion': True}},
            'bibliographystyle': { 're': re.compile(_latex_re_string('bibliographystyle')), 'kwargs': {'recursion': True}},
            'inputiffileexists': { 're': re.compile(_latex_re_string('InputIfFileExists')), 'kwargs': {'recursion': True}},
        }
